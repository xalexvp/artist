import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorsService } from '../../services/errors.service';

@Component({
  selector: 'app-errors',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.scss']
})
export class ErrorsComponent implements OnInit {

  locationSubscription: any;

  constructor( public errorsService: ErrorsService,
               private activatedRoute: ActivatedRoute,
               private router: Router ) { }

  doAfterError() {
    this.router.navigate(['/admin']);
    this.errorsService.isError = false;
    this.locationSubscription = this.activatedRoute.firstChild.url.subscribe(res => {
      setTimeout(() => {
        this.router.navigate([res[0].path]);
      }, 20);
    });
    this.locationSubscription.unsubscribe();
  }

  ngOnInit() {
  }
}

