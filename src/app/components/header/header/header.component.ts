import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  showGalleryButton = true;
  showTitle = true;
  showContactsButton = false;
  showCompetitionsButton = false;
  startPic: boolean;

  constructor( public router: Router ) {
    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {

          switch (event.urlAfterRedirects) {
            case '':
              this.showGalleryButton = true;
              this.showContactsButton = false;
              this.showTitle = true;
              break;

            case '/gallery':
              this.showGalleryButton = false;
              this.showContactsButton = true;
              this.showTitle = false;
              break;

            case '/contacts':
              this.showGalleryButton = true;
              this.showContactsButton = false;
              this.showTitle = false;
              break;

            case '/competitions':
              this.showGalleryButton = true;
              this.showCompetitionsButton = true;
              this.showContactsButton = true;
              this.showTitle = false;
              break;

            default:
              this.showGalleryButton = true;
              this.showCompetitionsButton = false;
              this.showContactsButton = false;
              this.showTitle = true;
              return;
          }
        }
      });
  }

  ngOnInit() {
    setTimeout(() => this.startPic = true, 0);
  }
}
