import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/internal/operators';
import { ErrorsService } from './errors.service';

@Injectable({
  providedIn: 'root'
})
export class SendDataService {
  // varURL = this.channelService.getURL();
  // feedbackURL = 'handlers/feedback.php';
  feedbackURL = 'feedback.php';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
      // 'Authorization': sendData.key
    })
  };

  constructor( private http: HttpClient,
               private errorsService: ErrorsService ) {  }

  public sendFeedback(data) {

    return this.http.post( this.feedbackURL, JSON.stringify(data), this.httpOptions)

      .pipe(
        catchError((err: HttpErrorResponse) => {
          this.errorsService.showError(err.error);
          return new Promise(() => err.error);
        }),
        map((result: any) => {

          if (!result) {
            this.errorsService.showError({error: 'Error', message: 'Нет ответа от сервера', code: '01234'});
            return {};

          } else {
            return result;
          }
        })
      );
  }
}

