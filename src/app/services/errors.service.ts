import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorsService {

  errorObj: any = {};
  APIError: any = {};
  isError = false;
  errorMessage = '';
  errorMessage2: string;
  errorFullMessage: string;

  constructor( ) { }

  public showError(inError) {

    this.errorObj = inError;
    this.errorObj.message = inError.message;
    this.errorMessage2 = '';

    // this.errorObj.actionAfter = inError.actionAfter;
    if (this.errorObj.hasOwnProperty('code')) {
      if (this.errorObj.hasOwnProperty('message')) {
        this.errorMessage = this.errorObj.message;
      }
      this.isError = true;
    }
  }
}
