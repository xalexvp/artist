import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';

import { GalleryRoutingModule } from './gallery-routing.module';
import { GalleryComponent } from './gallery/gallery.component';
import { HeaderModule } from '../../components/header/header.module';

@NgModule({
  declarations: [GalleryComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    GalleryRoutingModule,
    MatButtonModule,
    HeaderModule
  ]
})
export class GalleryModule { }
