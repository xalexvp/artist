import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  selectedNumber = 0;
  selected: boolean;
  galleryLength = 95;
  paintings = [];
  orderPictureForm: FormGroup;
  showFeedback: boolean;
  showOrder: boolean;
  sizes = {
    min: '20x30',
    preMid: '25x35',
    middle: '30x45',
    preBig: '35x50',
    big: '40x60',
    largest: '60x80'
  };

  constructor(private fb: FormBuilder) {
    this._createForm();
  }

  ngOnInit() {
    for (let i = this.galleryLength; i > 0; i--) {
      this.paintings.push(i);
    }
  }

  public selectPic(event) {
    // console.dir(event);
    if (!event.target.alt || (typeof parseInt(event.target.alt, 10) !== 'number')) {
      return;
    }
    if (this.selected && this.selectedNumber) {
      this.unselectPic();
    } else {
      this.selectedNumber = parseInt(event.target.alt, 10);
      this.selected = true;
    }
  }
  public unselectPic() {
    this.selected = false;
    this.selectedNumber = 0;
  }

  private _createForm() {
    this.orderPictureForm = this.fb.group({
      pictureNumber: this.selectedNumber,
      pictureSize: this.sizes.min
    });
  }

  public openFeedback() {
    // this.showFeedback = true;
  }
  public openOrder() {
    // this.showOrder = true;
  }
}
