import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompetitionsRoutingModule } from './competitions-routing.module';
import { CompetitionsComponent } from './competitions/competitions.component';
import { HeaderModule } from '../../components/header/header.module';

@NgModule({
  declarations: [CompetitionsComponent],
  imports: [
    CommonModule,
    CompetitionsRoutingModule,
    HeaderModule
  ]
})
export class CompetitionsModule { }
