import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-competitions',
  templateUrl: './competitions.component.html',
  styleUrls: ['./competitions.component.scss']
})
export class CompetitionsComponent implements OnInit {

  diplomasCounter = 2;
  diplomas = [];

  constructor() { }

  ngOnInit(): void {
    for (let i = this.diplomasCounter; i > 0; i--) {
      this.diplomas.push(i);
    }
  }

}
