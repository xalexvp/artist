import { Component } from '@angular/core';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent {
  phoneNumber = '+38(066)488-30-02';
  eMail = 'lider.agro.centr@gmail.com';
}
