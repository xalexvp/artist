import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './views/main/main.component';

const routes: Routes = [
  { path: '', component: MainComponent },
  {
    path: 'gallery',
    loadChildren: () => import('./views/gallery/gallery.module').then(m => m.GalleryModule)
  },
  {
    path: 'contacts',
    loadChildren: () => import('./views/contacts/contacts.module').then(m => m.ContactsModule)
  },
  {
    path: 'competitions',
    loadChildren: () => import('./views/competitions/competitions.module').then(m => m.CompetitionsModule)
  },
  { path: '**',
    redirectTo: '', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
